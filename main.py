from settings import Settings
from asset_management import AssetCatalog, mainAssetCatalog

if __name__ == "__main__":
    # Try to load settings
    Settings.load("settings.json")
    mainAssetCatalog = AssetCatalog({
        "location": Settings.global_data["assets"]["catalog"]
    })
    print(mainAssetCatalog["testasset"].get_asset())
    mainAssetCatalog.release()
    print("WJ Engine :D")