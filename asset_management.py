from os.path import join
from json import load

from settings import Settings

class Asset:
    """
    Base class for all asset types
    """

    def __init__(self, settings:dict={}):
        self.ready = False

    def load(self):
        """
        Loads this asset
        """
        self.ready = True

    def release(self):
        """
        Releases data loaded by this asset
        """
        self.ready = False

    def get_asset(self):
        """
        Returns asset data; it is loaded earlier if necessary
        :return: Asset data
        """
        if not self.ready:
            self.load()
        return None


class FileAsset(Asset):
    """
    Asset that holds text file
    """

    def __init__(self, settings:dict={}):
        super(FileAsset, self).__init__(settings)
        self.location = join(Settings.global_data["assets"]["directory"], settings["location"])
        self.file = None


    def load(self):
        """
        Loads text file
        """
        self.file = open(self.location)
        super(FileAsset, self).load()

    def release(self):
        """
        Closes text file
        """
        if self.file is not None:
            self.file.close()
        super(FileAsset, self).release()

    def get_asset(self):
        """
        Get text file
        """
        super(FileAsset, self).get_asset()
        return self.file


class JSONAsset(FileAsset):
    """
    Asset that holds JSON data
    """

    def load(self):
        """
        Loads and parses JSON file
        """
        super(JSONAsset, self).load()
        self.parsed = load(self.file)
        
    def release(self):
        self.parsed = None
        super(JSONAsset, self).release()

    def get_asset(self):
        """
        Get parsed JSON data from given file
        :return:
        """
        super(JSONAsset, self).get_asset()
        return self.parsed


class AssetCatalog(JSONAsset):
    """
    Asset that catalogs other assets
    """

    def load(self):
        super(AssetCatalog, self).load()
        self.assets = {}
        for (key, value) in self.parsed.items():
            print(key)
            assetType = ASSET_TYPES[value["type"]]
            self.assets[key] = assetType(settings=value)

    def release(self):
        for x in self.assets.values():
            x.release()
        super(AssetCatalog, self).release()

    def get_asset(self):
        super(AssetCatalog, self).get_asset()
        return self.assets

    def __getitem__(self, item):
        if not self.ready:
            self.load()

        return self.assets[item]


"""
Main asset catalog
"""
mainAssetCatalog = None

ASSET_TYPES = {
    "Generic": Asset,
    "File": FileAsset,
    "JSON": JSONAsset,
    "Catalog": AssetCatalog
}