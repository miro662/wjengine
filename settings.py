from json import load

class Settings:
    """
    Class storing engine-wide settings
    """

    global_data = {}

    def load(settingsFileLocation):
        """
        Loads settings file
        """
        file = open(settingsFileLocation)
        Settings.global_data = load(file)

